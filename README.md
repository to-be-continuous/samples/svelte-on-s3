# Svelte project sample

This project sample shows the usage of _to be continuous_ templates:

* build, test &amp; analyse with `npm`
* analyse with sonar (from [sonarcloud.io](https://sonarcloud.io/))
* deploy the built static resources on S3 with webhosting

## Node template feature

This project uses the following features from the To be continuous **Node** template:

* Perform build by setting the `NODE_BUILD_DIR` variable to `build`
* Implements Vitest based unit tests
* Perform a lint job by setting the `NODE_LINT_ENABLED` variable to `true`
* Perform an audit job and an outdated job

The to-be-continuous Node template also implements unit tests and code coverage integration in GitLab:

* unit tests results and code coverage are automatically integrated in merge requests,
* code coverage badge (see above).

## SonarQube template features

This project uses the following features from the To be continuous **SonarQube** template:

* Defines the `SONAR_HOST_URL` (SonarQube server host),
* Defines `organization` & `projectKey` from [sonarcloud.io](https://sonarcloud.io/) in `sonar-project.properties`,
* Defines :lock: `$SONAR_TOKEN` as secret CI/CD variable,
* Uses the `sonar-project.properties` to specify project specific configuration:
  * source and test folders,
  * html support for .svelte files,
  * code coverage report,
  * linter report,
  * unit test reports.

> :warning: **This is only a partial analysis.**
> 
> SonarQube does not yet support the Svelte framework.
> While waiting for an update, we configure the tool to perform an html analysis of .svelte files.

## S3 template features

This project uses the following features from the To be continuous **S3** template:

* Defines mandatory `$S3_ENDPOINT_HOST`,
* Defines mandatory `$S3_ACCESS_KEY` and `$S3_SECRET_KEY` as secrets project variables,
* Set `$S3_DEPLOY_FILES` to define the Svelte output build directory as the deployable S3 resources.

The to-be-continuous S3 template also implements environments integration in GitLab:

* deployment environment integrated in merge requests,
* review environment cleanup support (manually or when the related development branch is deleted).

## Svelte project details

This project is based on a basic Svelte application that use:

* [npm](https://www.npmjs.com/), 
* [eslint](https://eslint.org/), 
* [SvelteKit](https://svelte.dev/docs/kit/introduction),
* [vite](https://vite.dev/)
* and [vitest](https://vitest.dev/).

### About unit testing

There is no official testing library for Svelte, but in [Svelte FAQ](https://svelte.dev/faq#how-do-i-test-svelte-apps), they suggest to use [Svelte Testing Library](https://testing-library.com/docs/svelte-testing-library/example/).

The [Svelte Testing Library](https://testing-library.com/docs/svelte-testing-library/setup) setup recommend to use [vitest]https://vitest.dev/) but you're free to use the library with any testing framework and runner you're comfortable with.

You can find [here](https://github.com/vitest-dev/vitest/tree/main/examples/sveltekit) a quick tutorial to How to test Svelte components with Svelte Testing Library and Vitest.

### About lint

The official ESLint for Svelte is [eslint-plugin-svelte](https://github.com/sveltejs/eslint-plugin-svelte)

Add the package as a development dependency:

```bash
npm install --save-dev eslint-plugin-svelte
```

Then add a file `eslint.config.js` to configure lint rules

For example:

```javascript
import eslintPluginSvelte from 'eslint-plugin-svelte';
import js from '@eslint/js';
import ts from '@typescript-eslint/eslint-plugin';
import tsParser from '@typescript-eslint/parser';

export default [
  // Generic recommended settings
  js.configs.recommended,

  // Svelte-specific linting rules
  ...eslintPluginSvelte.configs['flat/recommended'],

  {
    files: ['*.ts', '*.svelte'], // Include TS and Svelte files
    languageOptions: {
      parser: tsParser,
      parserOptions: {
        tsconfigRootDir: './',
        project: './jsconfig.json',
      },
    },
    plugins: {
      '@typescript-eslint': ts,
    },
    rules: {
      // Add or override rules specific to TypeScript
      '@typescript-eslint/no-unused-vars': 'warn',
      '@typescript-eslint/no-explicit-any': 'warn',
    },
  },
];
```

## Launch in local 

### Requirements

* Node Package Manager - npm - [https://www.npmjs.com/](https://www.npmjs.com/)

### Getting Started

* Clone the repo
* Install dependencies with `npm install`

### Development Server

* Run development server with `npm run dev` and go here:
  [http://localhost:8080/](http://localhost:8080/)

The app will automatically reload if you change any of the source files

### Build

Run `npm run build` to build the project. The build artifacts will be stored in **build**.

### Running Tests

Run `npm run test` to perform unit tests of the project.

### Lint

Run `npm run lint` to perform a lint analysis of the code.

### Audit

Run `npm audit` to perform an audit of the code.

### Outdated

Run `npm outdated` to check if any installed packages are currently outdated.
