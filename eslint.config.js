import eslintPluginSvelte from 'eslint-plugin-svelte';
import js from '@eslint/js';
import ts from '@typescript-eslint/eslint-plugin';
import tsParser from '@typescript-eslint/parser';

export default [
  // Generic recommended settings
  js.configs.recommended,

  // Svelte-specific linting rules
  ...eslintPluginSvelte.configs['flat/recommended'],

  {
    files: ['*.ts', '*.svelte'], // Include TS and Svelte files
    languageOptions: {
      parser: tsParser,
      parserOptions: {
        tsconfigRootDir: './',
        project: './jsconfig.json',
      },
    },
    plugins: {
      '@typescript-eslint': ts,
    },
    rules: {
      // Add or override rules specific to TypeScript
      '@typescript-eslint/no-unused-vars': 'warn',
      '@typescript-eslint/no-explicit-any': 'warn',
    },
  },
];
