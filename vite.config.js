import { sveltekit } from "@sveltejs/kit/vite";
import { svelteTesting } from '@testing-library/svelte/vite';

/** @type {import('vite').UserConfig} */
const config = {
  plugins: [sveltekit(), svelteTesting()],
  test: {
    globals: true,
    environment: 'jsdom',
    setupFiles: ['./src/setupTests.ts'],
    reporters: [
        'default', 
        ['vitest-sonar-reporter', { outputFile: './reports/node-test.xunit.xml' }],
    ], // Add sonar reporter
    coverage: {
        provider: 'v8',
        reportsDirectory: './reports',
        reporter: ['text', 'lcov'],
        include: ['src/**/*.svelte'],
    }
  },
};

export default config;