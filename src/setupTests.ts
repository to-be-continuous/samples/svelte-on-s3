import '@testing-library/jest-dom';
import { vitest, afterEach } from 'vitest';
import { cleanup } from '@testing-library/svelte';

// Mock matchMedia
Object.defineProperty(window, 'matchMedia', {
  writable: true,
  value: vitest.fn().mockImplementation(query => ({
    matches: false,
    media: query,
    onchange: null,
    addListener: vitest.fn(),
    removeListener: vitest.fn(),
    addEventListener: vitest.fn(),
    removeEventListener: vitest.fn(),
    dispatchEvent: vitest.fn(),
  })),
});

// Cleanup after each test
afterEach(() => {
  cleanup();
});