import '@testing-library/jest-dom'
import Counter from '../Counter.svelte'
import { test, expect } from 'vitest';
import { render, fireEvent } from '@testing-library/svelte'

test('Button click works', async () => {
    const { getByTestId } = render(Counter)

    const button = getByTestId('button')
    const counter = getByTestId('counter')

    await fireEvent.click(button)
    await fireEvent.click(button)
    await fireEvent.click(button)

    expect(counter.textContent).toBe('3')

    // with jest-dom
    expect(counter).toHaveTextContent('3')
})
